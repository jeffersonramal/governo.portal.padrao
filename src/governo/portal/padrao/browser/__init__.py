# -*- coding: utf-8 -*-
from governo.portal.padrao.browser.filterresults import FilterResultsView  # noqa: E501,F401
from governo.portal.padrao.browser.searchlibrary import SearchLibraryView  # noqa: E501,F401
from governo.portal.padrao.browser.news import NewsView  # noqa: E501,F401
