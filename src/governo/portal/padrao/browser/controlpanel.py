# -*- coding: utf-8 -*-
from governo.portal.padrao import _
from governo.portal.padrao.config import DEFAULT_DOCUMENT_TYPES
from plone.app.registry.browser import controlpanel
from plone.autoform import directives as form
from plone.supermodel import model
from zope import schema


class IPortalPadraoSettings(model.Schema):
    """Interface for the control panel form."""

    form.widget('available_document_types', rows=7)
    available_document_types = schema.List(
        title=_(u'Available Document Types'),
        description=_(u'List of available document types in the site.'),
        required=True,
        default=DEFAULT_DOCUMENT_TYPES,
        value_type=schema.TextLine(title=_(u'Document Type')),
    )


class PortalPadraoSettingsEditForm(controlpanel.RegistryEditForm):
    schema = IPortalPadraoSettings
    label = _(u'Portal Padrao settings')
    description = _(u'Here you can modify settings for the site.')


class PortalPadraoSettingsControlPanel(controlpanel.ControlPanelFormWrapper):
    form = PortalPadraoSettingsEditForm
